var ruleState = {
    preload:function(){
	},
	create:function(){

		this.menubgmMusic = game.add.audio('menubgmMusic');
    	this.menubgmMusic.loop = true;
		this.menubgmMusic.play();
		this.buttonMusic = game.add.audio('buttonMusic');
    	this.buttonMusic.loop = false;
		//---------------------------------about text
		this.background_color="#66ff99";
		this.font_color="#104E8B";
		game.stage.backgroundColor=this.background_color;
		game.add.tileSprite(0, 0, 1280, 640, 'rule');
		
		this.font_style={font:'40px Arial',fill:this.font_color,fontWeight:'bold'};
		this.font_style_picked={font:'40px Arial',fill:"#ff5809",fontWeight:'bold'};
		
		this.menu=game.add.text(game.width/2,500,'Menu',this.font_style);
		this.menu.anchor.setTo(0.5,0.5);
		
		this.menu_obj=[this.menu];
		//--------------------------------about character
		this.cover=[false,false,false];
		
	},
	update:function(){
		//for development
		//game.state.start('play');
		this.check();
	},
	check:function(){
		var x=game.input.mousePointer.x;
		var y=game.input.mousePointer.y;
		var press=game.input.activePointer.leftButton.isDown;
		var m=this.menu_obj;
		for(var i=0;i<m.length;++i){
			if(x>m[i].x-m[i].width/2&&x<m[i].x+m[i].width/2&&y>m[i].y-m[i].height/2&&y<m[i].y+m[i].height/2){
				m[i].setStyle(this.font_style_picked);
				this.cover[i]=true;
				if(press){
					this.menubgmMusic.stop();
					this.buttonMusic.play();
					game.state.start('menu');
				};
			}
			else{
				this.cover[i]=false;
				m[i].setStyle(this.font_style);
			}
		}
	},
};