var menuState = {
    preload:function(){
	},
	create:function(){

		this.menubgmMusic = game.add.audio('menubgmMusic');
    	this.menubgmMusic.loop = true;
		this.menubgmMusic.play();
		this.buttonMusic = game.add.audio('buttonMusic');
    	this.buttonMusic.loop = false;
		//---------------------------------about text
		this.background_color="#66ff99";
		this.font_color="#fff";
		game.stage.backgroundColor=this.background_color;
		game.add.tileSprite(0, 0, 1280, 640, 'menubg');

		this.title_style={font:'72px Arial',fill:"#ffd306",fontWeight:'bold'};

		this.title_game=game.add.text(game.width/2,70,'Good game',this.title_style);
		this.title_game.anchor.setTo(0.5,0.5);
		
		this.font_style={font:'30px Arial',fill:this.font_color};
		this.font_style_picked={font:'30px Arial',fill:"#ff5809",fontWeight:'bold'};
		
		this.one_game=game.add.text(game.width/2,225,'Cooperation mode(2p)',this.font_style);
		this.one_game.anchor.setTo(0.5,0.5);
		this.two_game=game.add.text(game.width/2,290,'Confrontation mode(2p)',this.font_style);
		this.two_game.anchor.setTo(0.5,0.5);

		this.how=game.add.text(game.width/2,355,'Game rule',this.font_style);
		this.how.anchor.setTo(0.5,0.5);

		this.ctrl=game.add.text(game.width/2,420,'Control instructions',this.font_style);
		this.ctrl.anchor.setTo(0.5,0.5);

		this.props=game.add.text(game.width/2,480,'Props instructions',this.font_style);
		this.props.anchor.setTo(0.5,0.5);
		
		this.lea_game=game.add.text(game.width/2,540,'Leave the game',this.font_style);
		this.lea_game.anchor.setTo(0.5,0.5);
		
		this.menu_obj=[this.one_game,this.two_game,this.how,this.ctrl,this.props,this.lea_game];
		//--------------------------------about character
		this.create_people();
		this.cover=[false,false,false];
		var n = Math.floor(Math.random() * (10));
		if(n%2)game.map_name = 'map2';
		else game.map_name = 'map';
		
	},
	update:function(){
		//for development
		//game.state.start('play');
		this.check();
		this.player_move(this.player1,1,0);
		this.player_move(this.player2,1,1);
		this.player_move(this.player3,1,2);
		this.player_move(this.player4,1,2);
		game.physics.arcade.collide(this.player3,this.player4);
	},
	player_move:function(player,dir,num){
		this.player_change_dir(player,dir,num);
		if(player.body.velocity.x<0&&player.body.velocity.y==0)player.animations.play('left');
		else if(player.body.velocity.x>0&&player.body.velocity.y==0)player.animations.play('right');
		else if(player.body.velocity.y<0&&player.body.velocity.x==0)player.animations.play('up');
		else player.animations.play('down');
	},
	player_change_dir:function(player,dir,num){
		if(dir==1){
			if(this.cover[num]){
				player.body.velocity.x=(player.body.velocity.x>0)?player.speed*3:-1*player.speed*3;
			}
			else{
				player.body.velocity.x=(player.body.velocity.x>0)?player.speed:-1*player.speed;
			}
			var edge_x=player.edge;
			if(player.x<edge_x)player.body.velocity.x=this.cover[num]?player.speed*3:player.speed;
			else if(player.x>game.width-edge_x)player.body.velocity.x=this.cover[num]?-1*player.speed*3:-1*player.speed;
		}
		else{
			var edge_y=player.edge;
			if(player.y<edge_y)player.body.velocity.y=player.speed;
			else if(player.y>game.height-edge_y)player.body.velocity.y=-1*player.speed;
		}
	},
	check:function(){
		var x=game.input.mousePointer.x;
		var y=game.input.mousePointer.y;
		var press=game.input.activePointer.leftButton.isDown;
		var m=this.menu_obj;
		for(var i=0;i<m.length;++i){
			if(x>m[i].x-m[i].width/2&&x<m[i].x+m[i].width/2&&y>m[i].y-m[i].height/2&&y<m[i].y+m[i].height/2){
				m[i].setStyle(this.font_style_picked);
				this.cover[i]=true;
				if(press){
					this.menubgmMusic.stop();
					this.buttonMusic.play();
					if(i==5)this.leave();
					else this.start(i+1);
				};
			}
			else{
				this.cover[i]=false;
				m[i].setStyle(this.font_style);
			}
		}
	},
    start:function(i){
		if(i==1){
			game.state.start('play');
			game.gamemode = 1;
		}
		else if(i==2){
			game.state.start('play2');
			game.gamemode = 2;
		}
		else if(i==3){
			game.state.start('rule');
			game.gamemode = 0;
		}
		else if(i==4){
			game.state.start('control');
			game.gamemode = 0;
		}
		else {
			game.state.start('props_i');
			game.gamemode = 0;
		}
    },
	leave:function(){
		var random = Math.floor((Math.random() * 100) + 1);
		if(random >= 1 && random < 25)
			window.location.href="https://105062218.gitlab.io/Assignment_02";
		else if(random >= 25 && random < 50)
			window.location.href="https://105062220.gitlab.io/Assignment_02";
		else if(random >= 50 && random < 75)
			window.location.href="https://105062227.gitlab.io/Assignment_02";
		else
			window.location.href="https://105062230.gitlab.io/Assignment_02";
		
	},
	create_people:function(){
		this.player1=game.add.sprite(500,140,'player');
		this.player1.anchor.setTo(0.5,0.5);
		game.physics.arcade.enable(this.player1);
		this.player1.animations.add('left',[0,1,2,3],10);
		this.player1.animations.add('right',[4,5,6,7],10);
		this.player1.animations.add('up',[8,9,10,11],10);
		this.player1.animations.add('down',[12,13,14,15],10);
		this.player1.speed=100;
		this.player1.edge=300;
		this.player1.body.velocity.x=-1*this.player1.speed;
		
		this.player2=game.add.sprite(1000,140,'player2');
		this.player2.anchor.setTo(0.5,0.5);
		game.physics.arcade.enable(this.player2);
		this.player2.animations.add('left',[0,1,2,3],10);
		this.player2.animations.add('right',[4,5,6,7],10);
		this.player2.animations.add('up',[8,9,10,11],10);
		this.player2.animations.add('down',[12,13,14,15],10);
		this.player2.speed=150;
		this.player2.edge=300;
		this.player2.body.velocity.x=this.player2.speed;
		
		this.player3=game.add.sprite(100,game.height-50,'player3');
		this.player3.anchor.setTo(0.5,0.5);
		game.physics.arcade.enable(this.player3);
		this.player3.animations.add('left',[0,1,2,3],10);
		this.player3.animations.add('right',[4,5,6,7],10);
		this.player3.animations.add('up',[8,9,10,11],10);
		this.player3.animations.add('down',[12,13,14,15],10);
		this.player3.speed=200;
		this.player3.edge=200;
		this.player3.body.velocity.x=this.player3.speed;
		this.player3.body.bounce.set(1);
		
		this.player4=game.add.sprite(1000,game.height-50,'player3');
		this.player4.anchor.setTo(0.5,0.5);
		game.physics.arcade.enable(this.player4);
		this.player4.animations.add('left',[0,1,2,3],10);
		this.player4.animations.add('right',[4,5,6,7],10);
		this.player4.animations.add('up',[8,9,10,11],10);
		this.player4.animations.add('down',[12,13,14,15],10);
		this.player4.speed=250;
		this.player4.edge=100;
		this.player4.body.velocity.x=-1*this.player4.speed;
		this.player4.body.bounce.set(1);
	},
};